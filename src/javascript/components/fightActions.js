import { controls } from '../../constants/controls';
import { showAttack, toggleShield, toggleSuperindicator } from './fightViewChanges';
const { PlayerOneAttack,
  PlayerTwoAttack,
  PlayerOneBlock,
  PlayerTwoBlock,
  PlayerOneCriticalHitCombination,
  PlayerTwoCriticalHitCombination } = controls;

let blockPlayerOneActivated = false;
let blockPlayerTwoActivated = false;
let timerPlayerTwoAttack;
let timerPlayerOneAttack;
let flagTimerPlayerOne = false;
let flagTimerPlayerTwo = false;

let flagTimerPlayerOneSuper = false;
let flagTimerPlayerTwoSuper = false;
let pressedComboOne = [];
let pressedComboTwo = [];


export async function keyDownActionHandler(key) {
  let actionResult = null;
  if(~Object.values(controls).indexOf(key)) {
    if((key === PlayerOneAttack && !blockPlayerOneActivated) || (key === PlayerTwoAttack && !blockPlayerTwoActivated)) {
      actionResult = await waitForDefense(key);
    }
    if(key === PlayerOneBlock) {
      blockPlayerOneActivated = true;
      toggleShield(blockPlayerOneActivated, 'left');
    }
    if(key === PlayerTwoBlock) {
      blockPlayerTwoActivated = true;
      toggleShield(blockPlayerTwoActivated, 'right');
    }
  }
  if(superShotAllowed(key, PlayerOneCriticalHitCombination, pressedComboOne, blockPlayerOneActivated, flagTimerPlayerOneSuper) ||
    superShotAllowed(key, PlayerTwoCriticalHitCombination, pressedComboTwo, blockPlayerTwoActivated, flagTimerPlayerTwoSuper)) {
    if(~PlayerOneCriticalHitCombination.indexOf(key)) {
      pressedComboOne.push(key);
    } else {
      pressedComboTwo.push(key);
    }
    actionResult = superShotAction();
  }
  return actionResult;
}

export function keyUpActionHandler(e) {
  const key = e.code;
  if(key === PlayerOneBlock) {
    blockPlayerOneActivated = false;
    toggleShield(blockPlayerOneActivated, 'left');
  }
  if(key === PlayerTwoBlock) {
    blockPlayerTwoActivated = false;
    toggleShield(blockPlayerTwoActivated, 'right');
  }
  if(~PlayerOneCriticalHitCombination.indexOf(key)) {
    pressedComboOne.splice(PlayerOneCriticalHitCombination.indexOf(key), 1);
  }
  if(~PlayerTwoCriticalHitCombination.indexOf(key)) {
    pressedComboTwo.splice(PlayerTwoCriticalHitCombination.indexOf(key), 1);
  }
}

async function waitForDefense(previousKey) {
  return new Promise((resolve) => {
    addEventListener('keydown', responseDefenseAction.bind(null, {previousKey, resolve}));
    if(!flagTimerPlayerOne && previousKey === PlayerOneAttack) {
      showAttack('left', 'fist');
      attackAction(timerPlayerOneAttack, 'playerOne', resolve);
    }
    if(!flagTimerPlayerTwo && previousKey === PlayerTwoAttack) {
      attackAction(timerPlayerTwoAttack, 'playerTwo', resolve);
      showAttack('right', 'fist');
    }
  });
}

function attackAction(timerName, attacker, resolve) {
  if(attacker === 'playerOne') {
    flagTimerPlayerOne = true;
  } else {
    flagTimerPlayerTwo = true;
  }
  timerName = setTimeout(() => {
    removeEventListener('keydown', responseDefenseAction);
    let result = {};
    result.superShot = false;
    result.attacker = attacker;
    if(attacker === 'playerOne') {
      flagTimerPlayerOne = false;
      result.block = blockPlayerTwoActivated;
    } else {
      flagTimerPlayerTwo = false;
      result.block = blockPlayerOneActivated;
    }
    resolve(result);
  }, 500);
}

function responseDefenseAction({previousKey, resolve}, e) {
  let result = {};
  if(previousKey === PlayerOneAttack && e.code === PlayerTwoBlock) {
    clearTimeout(timerPlayerOneAttack);
    result = {
      attacker: 'playerOne',
      block: true,
      superShot: false
    };
    resolve(result);
  }
  if(previousKey === PlayerTwoAttack && e.code === PlayerOneBlock) {
    clearTimeout(timerPlayerTwoAttack);
    result = {
      attacker: 'playerTwo',
      block: true,
      superShot: false
    };
    resolve(result);
  }
}

function superShotAllowed(key, criticalHitCombination, pressedCombo, block, flagTimerSuper) {
  if(!~criticalHitCombination.indexOf(key)) {
    return false;
  }
  if(~pressedCombo.indexOf(key)) {
    return false;
  }
  if(block) {
    return false;
  }
  return !flagTimerSuper;
}

function superShotAction() {
  const timerSuperShot = (player) => {
    if(player === 'playerOne') {
      showAttack('left', 'fireball');
      toggleSuperindicator(false, 'left');
      flagTimerPlayerOneSuper = true;
      setTimeout(() => {
        flagTimerPlayerOneSuper = false;
        toggleSuperindicator(true, 'left');
      }, 10000);
    } else {
      showAttack('right', 'fireball');
      toggleSuperindicator(false, 'right');
      flagTimerPlayerTwoSuper = true;
      setTimeout(() => {
        flagTimerPlayerTwoSuper = false;
        toggleSuperindicator(true, 'right');
      }, 10000);
    }
  };
  if(pressedComboOne.length === PlayerOneCriticalHitCombination.length) {
    pressedComboOne = [];
    timerSuperShot('playerOne');
    return {
      attacker: 'playerOne',
      block: false,
      superShot: true
    }
  }
  if(pressedComboTwo.length === PlayerTwoCriticalHitCombination.length) {
    pressedComboTwo = [];
    timerSuperShot('playerTwo');
    return {
      attacker: 'playerTwo',
      block: false,
      superShot: true
    }
  }
  return null;
}
