
export function changeHealthbarWidth(initialWidth, initialHealth, currentHealth, position) {
    const healthbar = document.getElementById(`${position}-fighter-indicator`);
    const newHealthbar = (initialWidth * currentHealth / initialHealth);
    const healthbarWidth = newHealthbar >= 0 ? newHealthbar : 0;
    healthbar.style.width = `${healthbarWidth}px`;
}

export function toggleShield(show, position) {
    const shield = document.getElementById(`${position}-shield`);
    if(show) {
        shield.style.visibility = 'visible';
    } else {
        shield.style.visibility = 'hidden';
    }
}

export function showAttack(position, attack) {
    const attackEl = document.getElementById(`${position}-${attack}`);
    attackEl.classList.add(`arena___${position}-${attack}-show`);
    setTimeout(() => {
        attackEl.classList.remove(`arena___${position}-${attack}-show`);
    }, 300);
}

export function toggleSuperindicator(show, position) {
    const indicator = document.getElementById(`${position}-superindicator`);
    if(show) {
        indicator.style.visibility = 'visible';
    } else {
        indicator.style.visibility = 'hidden';
    }
}
