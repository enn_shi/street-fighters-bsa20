import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if(fighter) {
    const fighterImg = createFighterImage(fighter);
    const fighterName = createElement({ tagName: 'h4' });
    const fighterDetails = createElement({
      tagName: 'div',
      className: 'fighter-details'
    });
    const fighterDetailsWrapper = createElement({
      tagName: 'div',
      className: 'fighter-details-wrapper'
    });
    fighterDetails.innerHTML = `
      <div class="fighter-detail-cell">
        <p><svg id="Capa_1" class="icon" enable-background="new 0 0 512 512" height="512" viewBox="0 0 512 512" width="512" xmlns="http://www.w3.org/2000/svg"><g><path d="m182.058 375.046-22.552-22.552-100.607 72.3 36.705 36.705z" fill="#8f4813"/><path d="m43.872 368.499h122.264v31.893h-122.264z" fill="#c47e4e" transform="matrix(.707 -.707 .707 .707 -241.089 186.85)"/><path d="m512 0-366.98 338.673 62.263 56.25 304.717-304.717z" fill="#b4d2d7"/><path d="m421.794 0-304.718 304.718 42.097 48.109 352.827-352.827z" fill="#e1ebf0"/><path d="m302.923 452.414 6.248-29.342c-48.658-10.361-92.972-34.432-128.15-69.61l-11.241-11.242-17.683 3.53-3.53 17.683 11.241 11.241c39.286 39.287 88.775 66.169 143.115 77.74z" fill="#87a0af"/><path d="m158.538 330.979c-35.178-35.178-59.249-79.491-69.61-128.149l-29.342 6.248c11.572 54.341 38.453 103.829 77.739 143.115l11.242 11.242 21.213-21.213z" fill="#b4d2d7"/><path d="m90.202 421.798-51.517 23.209-23.209 51.516c9.979 9.981 23.249 15.477 37.362 15.477 14.115 0 27.383-5.496 37.363-15.476s15.476-23.249 15.476-37.363c.001-14.114-5.496-27.383-15.475-37.363z" fill="#ff9f22"/><path d="m52.839 406.322c-14.114 0-27.383 5.496-37.363 15.476s-15.476 23.249-15.476 37.364c0 14.113 5.496 27.382 15.476 37.362l74.726-74.726c-9.981-9.98-23.249-15.476-37.363-15.476z" fill="#ffcd00"/></g></svg>
        ${fighter.attack}</p>
      </div>
      <div class="fighter-detail-cell">
        <p><svg version="1.1" id="Capa_1" class="icon" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <path style="fill:#61DE56;" d="M461.144,60.883L260.311,0.633c-2.812-0.844-5.809-0.844-8.621,0L50.856,60.883c-6.345,1.903-10.689,7.743-10.689,14.367v220.916c0,28.734,11.632,58.149,34.573,87.425c17.521,22.36,41.763,44.814,72.049,66.736c50.877,36.828,100.975,59.42,103.082,60.363c1.95,0.873,4.04,1.31,6.13,1.31c2.09,0,4.18-0.436,6.13-1.31c2.107-0.943,52.205-23.535,103.082-60.363c30.285-21.923,54.525-44.376,72.047-66.736c22.941-29.276,34.573-58.69,34.573-87.425V75.25C471.833,68.626,467.488,62.786,461.144,60.883z"/><path style="fill:#13C37B;" d="M461.144,60.883L260.311,0.633C258.905,0.211,257.452,0,256,0v512h0.001c2.09,0,4.18-0.436,6.13-1.31c2.107-0.943,52.205-23.535,103.082-60.363c30.285-21.923,54.525-44.376,72.047-66.736c22.941-29.276,34.573-58.69,34.573-87.425V75.25C471.833,68.626,467.488,62.786,461.144,60.883z"/><g></g></svg>
        ${fighter.defense}</p>
      </div>
      <div class="fighter-detail-cell">
        <p><svg version="1.1" id="Capa_1" class="icon" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><path style="fill:#FD3018;" d="M256,481c-3.53,0-7.046-1.23-9.873-3.706c-21.826-19.087-42.583-36.782-62.109-53.423C73.817,329.96,0,267.909,0,177.514C0,93.988,59.037,31,136,31c60.659,0,99.595,42.378,120,80.537C276.405,73.378,315.341,31,376,31c76.963,0,136,62.988,136,146.514c0,90.396-73.817,152.446-184.018,246.357c-19.526,16.641-40.283,34.336-62.109,53.423C263.046,479.77,259.53,481,256,481z"/><path style="fill:#E61E14;" d="M265.873,477.294c21.826-19.087,42.583-36.782,62.109-53.423C438.183,329.96,512,267.909,512,177.514C512,93.988,452.963,31,376,31c-60.659,0-99.595,42.378-120,80.537V481C259.53,481,263.046,479.77,265.873,477.294z"/><g></g></svg>
        ${fighter.health}</p>
      </div>
    `;
    fighterName.innerText = fighter.name;
    fighterDetailsWrapper.append(fighterName, fighterDetails);
    fighterElement.append(fighterImg, fighterDetailsWrapper);
  }
    return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
