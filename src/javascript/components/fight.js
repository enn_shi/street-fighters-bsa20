import { controls } from '../../constants/controls';
import { keyDownActionHandler, keyUpActionHandler } from './fightActions';
import { changeHealthbarWidth } from './fightViewChanges';

let initial = {};
const winner = {
  fighterWon: {},
  winnerListener: function(fighter) {},
  set fighter(val) {
    this.fighterWon = val;
    this.winnerListener(val);
  },
  addListener: function(listener) {
    this.winnerListener = listener;
  }
};

export async function fight(firstFighter, secondFighter) {
  setInitialState(firstFighter, secondFighter);
  const keydownHandlerBinded = keyDownHandler.bind(null, {firstFighter, secondFighter});
  document.addEventListener('keydown', keydownHandlerBinded);
  document.addEventListener('keyup', keyUpActionHandler);
  return new Promise((resolve) => {
      // resolve the promise with the winner when fight is over
    winner.addListener((fighter) => {
      document.removeEventListener('keydown', keydownHandlerBinded);
      document.removeEventListener('keyup', keyUpActionHandler);
      resolve(fighter);
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if(damage < 0) {
    return 0;
  }
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  if(!fighter) {
    return 0;
  }
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}

function getSuperShotPower(fighter) {
  return fighter.attack * 2;
}

async function keyDownHandler({firstFighter, secondFighter}, e) {
  const actionResult = await keyDownActionHandler(e.code);
  if(actionResult) {
    const {attacker, block, superShot} = actionResult;
    countDamage(firstFighter, secondFighter, attacker, block, superShot);
  }
  if (firstFighter.health <= 0) {
    winner.fighter = secondFighter;
  }
  if (secondFighter.health <= 0) {
    winner.fighter = firstFighter;
  }
}

function countDamage(firstFighter, secondFighter, attacker, block, superShot) {
  const damageMath = (attacker, defender) => {
    if(superShot) {
      defender.health -= getSuperShotPower(attacker);
    } else {
      defender.health -= getDamage(attacker, defender);
    }
  };
  if(attacker === 'playerOne') {
    if(!block || superShot) {
      damageMath(firstFighter, secondFighter);
      changeHealthbarWidth(initial.width, initial.playerTwo, secondFighter.health, 'right');
    }
  } else {
    if(!block || superShot) {
      damageMath(secondFighter, firstFighter);
      changeHealthbarWidth(initial.width, initial.playerOne, firstFighter.health, 'left');
    }
  }
}

function setInitialState (firstFighter, secondFighter) {
  initial = {
    playerOne: firstFighter.health,
    playerTwo: secondFighter.health,
    width: document.getElementById('left-fighter-indicator').offsetWidth
  };
}
